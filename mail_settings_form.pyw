#!/usr/bin/env python
# -*- coding: utf-8 -*-

from PyQt4 import QtCore, QtGui, uic
import pickle, shelve
import re
import subprocess
#import stylesheet_rc3
import email
from email.MIMEText import MIMEText
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email import encoders
import smtplib
import mimetypes
import threading
import logging
import os

def SaveSettings(key, account):#(*args, **kwargs)
    """Консервирует объект в бинарный(двоичный) файл"""
    #полка s по функциональности не отличается от словаря
    s = shelve.open('settings')
    #s['account'] = account
    s[key] = account
    s.close()

def LoadSettings(key):
    """Расконсервирует объект из файла в его исходное состояние"""
    r = shelve.open('settings')
    #print r[key]['mail_server']
    #print type(r)#'instance'
    #for x in r: self.comboBox.addItem(x)#print x, len(x)
    m = r[key]
    r.close()
    #print m
    return m

def addEmailInAddressBook(address):
    """Консервирует объект в бинарный(двоичный) файл"""
    s = shelve.open('abook')
    s[address[1]] = address #address[1] - key
    s.close()
'''addEmailInAddressBook([u'Юрка 2', 'jklppt2@rambler.ru'])
r = shelve.open('abook')
for x in r: print x, r[x][0], r[x][1]
#print r['mymail@newbox.com'][0]
#print r['my@box.com'][1]
r.close()'''
def centerWindow(window):
    window.move(window.width() * -2, 0)
    window.show()
    desktop = QtGui.QApplication.desktop()
    x = (desktop.width() - window.width()) // 2
    y = (desktop.height() - window.height()) // 2
    window.move(x, y)
#help(QtGui.QTableWidget)#help(QtGui.QWidget)
#help(QtGui.QTableWidgetItem.setBackgroundColor)
class QSelectPath(QtGui.QWidget):
    """
    Виджет для выбора пути к файлу
    """
    def __init__(self, file_or_folder, parent=None):
        super(QSelectPath, self).__init__(parent)
        self.path = None
        self.file_or_folder = file_or_folder
        self.build_widget()
 
    def build_widget(self):
        main_layout = QtGui.QHBoxLayout()
 
        # Убираем внешние отступы у компановщика
        main_layout.setContentsMargins(0, 0, 0, 0)
 
        self.path = QtGui.QLineEdit(self)
        self.path.setReadOnly(True)
        self.path.setFocusPolicy(QtCore.Qt.NoFocus)
 
        overview = QtGui.QPushButton(u'Обзор...', self)
        overview.setFocusPolicy(QtCore.Qt.NoFocus)
        self.connect(overview, QtCore.SIGNAL('clicked()'), self.choose_file)
 
        main_layout.addWidget(self.path)
        main_layout.addWidget(overview)
 
        self.setLayout(main_layout)
 
    def choose_file(self):
        """
        Открывает диалоговое окно выбора папки
        """
        if self.file_or_folder == 'file':
            path = QtGui.QFileDialog.getOpenFileName(self, u'Выберите файл')
        else:
            path = QtGui.QFileDialog.getExistingDirectory(self, u'Выберите папку')

        if path:
            self.path.setText(path)
 
    def text(self):
        return unicode(self.path.text())
 
    def set_text(self, string):
        self.path.setText(string)

def decorator(F):       #F - функция или метод, не связанный с экземпляром
    def wrapper(*args): # для методов - экземпляр класса в args[0]
        # F(*args) - вызов функции или метода
        logging.basicConfig(format='\n%(asctime)s %(levelname)s:%(message)s',filename="log.log")
        logging.exception('')
        '''try:
        except Exception:
            logging.basicConfig(format='\n%(asctime)s %(levelname)s:%(message)s',filename="log.log")
            logging.exception('')
            QtGui.QMessageBox.information(self, u"Инфо",u"Произошла ошибка при конвертации. Информация будет добавлена в журнал ошибок",buttons=QtGui.QMessageBox.Close,defaultButton=QtGui.QMessageBox.Close)
            self.ConvertTxtToPDFButton.setDisabled(False)
            sys.exit()'''
        my_thread = threading.Thread(target=F, args=args)
        my_thread.start()
    return wrapper

class Settings(QtGui.QMainWindow):
    def __init__(self):
        super(Settings, self).__init__()
        QtGui.QWidget.__init__(self)
        uic.loadUi("C:/mail_settings_form/views/settings.ui", self)#G:/test/views/settings.ui

        self.connect(self.SaveButton, QtCore.SIGNAL("clicked()"), self.save)
        self.connect(self.DeleteButton, QtCore.SIGNAL("clicked()"), self.delete)
        self.comboBox.currentIndexChanged.connect(self.selectionchange)

        self.comboBox.addItem(u'Новая запись')
        Settings.get_accounts_list(self)

        self.action_exit.triggered.connect(self.close)

        # или textEdited
        self.account.textChanged.connect(self.on_text_changed)
        self.mail_server.textChanged.connect(self.on_text_changed)
        self.mail_server_port.textChanged.connect(self.on_text_changed)
        self.login.textChanged.connect(self.on_text_changed)
        self.password.textChanged.connect(self.on_text_changed)

    @staticmethod
    def get_accounts_list(self):
        """_ объект из файла в его исходное состояние"""
        r = shelve.open('settings')
        for x in r: self.comboBox.addItem(x)
        r.close()
    #get_accounts_list=staticmethod(get_accounts_list)
        
    def selectionchange(self, i):
        if self.comboBox.currentIndex() == 0:
            self.clear_form()
            return
        #self.SaveButton.setDisabled(False)
        #удалить print type(str(self.comboBox.currentText()))
        s = LoadSettings(str(self.comboBox.currentText()))
        self.account.setText(s['account'])
        self.mail_server.setText(s['mail_server'])
        self.mail_server_port.setText(s['mail_server_port'])
        self.login.setText(s['login'])
        self.password.setText(s['password'])
        self.SaveButton.setDisabled(False)

    def clear_form(self):
        #map(lambda x: x.clear(), self.findChildren(QtGui.QLineEdit))
        for i in self.findChildren(QtGui.QLineEdit): i.clear()

    def delete(self):
        r = shelve.open('settings')
        #print r
        del r[str(self.comboBox.currentText())]
        #for x in r: self.comboBox.addItem(x)#print x, len(x)
        #m = r[key]
        r.close()
        self.comboBox.removeItem(self.comboBox.currentIndex())
        #print LoadSettings(str(self.account.text()))
        #print type(account)

    def save(self):
        for i in self.findChildren(QtGui.QLabel): i.setStyleSheet("color: #000000;")
        if re.match("^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)$", str(self.account.text())) == None:
            self.label_6.setStyleSheet("color: #ff0000;")#или self.mail_server_port.setStyleSheet("background-color: #ff0000;")
            return
        #если порт это не целое число
        try:
            int(self.mail_server_port.text())
        except ValueError:
            self.label_3.setStyleSheet("color: #ff0000;")
            return
        #словарь
        account = {
            'account':str(self.account.text()),
            'mail_server':str(self.mail_server.text()),
            'mail_server_port':str(self.mail_server_port.text()),
            'login':str(self.login.text()),
            'password':str(self.password.text())
        }
        SaveSettings(str(self.account.text()), account)
        #выполнить только в случае успешного сохр нов уч записи self.comboBox.addItem(str(self.account.text()))
        self.comboBox.addItem(self.account.text())
        for i in self.findChildren(QtGui.QLabel): i.setStyleSheet("color: #000000;")
        self.SaveButton.setDisabled(True)

    def on_text_changed(self):
        #if self.SaveButton.isEnabled() == False:#Кнопка нажата
        self.SaveButton.setDisabled(False)


class addressBook(QtGui.QWidget):
    def __init__(self, parent=None):
        super(addressBook, self).__init__(parent)

        self.table = QtGui.QTableWidget()
        self.table.setColumnCount(2)
        self.table.setHorizontalHeaderLabels([u"Имя", "Email"])
        self.table.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
        #table.setVerticalHeaderLabels(["1", "2", "3"])
        #table.verticalHeader().setVisible(False)
        self.table.resize(150, 50)

        r = shelve.open('abook')
        self.table.setRowCount(len(r))
        for i, row in enumerate(sorted(r, key=r.__getitem__)):#не работает sorted(r, reverse=True)_key=lambda x,y: cmp(r[x], r[y])
            for column in range(2):
                self.table.setItem(i,column, QtGui.QTableWidgetItem(r[row][0] if column == 0 else r[row][1]))
        r.close()

        addButton = QtGui.QPushButton(u'Добавить контакт', self)
        self.connect(addButton, QtCore.SIGNAL('clicked()'), self.add)

        editButton = QtGui.QPushButton(u'Изменить контакт', self)
        self.connect(editButton, QtCore.SIGNAL('clicked()'), self.edit)

        delButton = QtGui.QPushButton(u'Удалить контакт', self)
        self.connect(delButton, QtCore.SIGNAL('clicked()'), self.delete)

        self.table.resizeColumnToContents(0)
        self.table.horizontalHeader().setStretchLastSection(True)

        layout = QtGui.QGridLayout()
        layout.addWidget(self.table, 0, 0)
        layout.setSpacing(15)
        layout.addWidget(addButton)
        layout.addWidget(editButton)
        layout.addWidget(delButton)
        self.setLayout(layout)

        self.setWindowTitle("Address Book")

        #self.table.cellDoubleClicked.connect(self.double_clicked)
        self.table.cellClicked.connect(self.cellClick)

    def add(self):
        self.window = addContactWindow()
        centerWindow(self.window)
    def addRow(self, name, Email):
        self.table.insertRow(self.table.rowCount())
        self.table.setItem(self.table.rowCount()-1,0, QtGui.QTableWidgetItem(name))
        self.table.setItem(self.table.rowCount()-1,1, QtGui.QTableWidgetItem(Email))
        #self.table.item(self.table.rowCount(), 1).setText('myText')

    def edit(self):
        self.window = changeContactWindow()
        self.window.setNewValues()
        centerWindow(self.window)

    def delete(self):
        #if not self.table.selectedIndexes():
        #return
        if len(self.table.selectedItems()) <> 2:
            return
        #print self.emailTo#self.table.currentRow() or selectedItems()
        #self.table.removeRow(self.table.currentRow())
        r = shelve.open('abook')
        del r[str(self.emailTo)]
        #self.table.removeRow
        #for x in r: self.comboBox.addItem(x)#print x, len(x)
        r.close()
        for index in self.table.selectedIndexes():
            self.table.removeRow(index.row())
            return

    def cellClick(self, row,col):
        self.row = row#запоминаем номер строки для использования в методе edit
        self.name = self.table.item(row, 0).text()
        if col == 0: col = col+1
        self.emailTo = self.table.item(row, col).text()

class addressbookWindow(QtGui.QWidget):
    def __init__(self, parent=None):
        super(addressbookWindow, self).__init__(parent)

        self.table = QtGui.QTableWidget()
        self.table.setColumnCount(2)
        self.table.setHorizontalHeaderLabels([u"Имя", "Email"])
        self.table.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
        #table.setVerticalHeaderLabels(["1", "2", "3"])
        #table.verticalHeader().setVisible(False)
        self.table.resize(150, 50)

        r = shelve.open('abook')
        self.table.setRowCount(len(r))
        for i, row in enumerate(sorted(r, key=r.__getitem__)):
            for column in range(2):
                self.table.setItem(i,column, QtGui.QTableWidgetItem(r[row][0] if column == 0 else r[row][1]))
        r.close()

        #addButton = QtGui.QPushButton(u'Добавить контакт', self)
        #self.connect(addButton, QtCore.SIGNAL('clicked()'), self.add)

        #delButton = QtGui.QPushButton(u'Удалить контакт', self)
        #self.connect(delButton, QtCore.SIGNAL('clicked()'), self.delete)

        butSelect = QtGui.QPushButton(u'Выбрать', self)
        self.connect(butSelect, QtCore.SIGNAL('clicked()'), self.butSelect)

        self.table.resizeColumnToContents(0)
        self.table.horizontalHeader().setStretchLastSection(True)

        layout = QtGui.QGridLayout()
        layout.addWidget(self.table, 0, 0)
        layout.setSpacing(15)
        #layout.addWidget(addButton)
        #layout.addWidget(delButton)
        layout.addWidget(butSelect)
        self.setLayout(layout)

        self.setWindowTitle("Address Book")

        #self.table.cellDoubleClicked.connect(self.double_clicked)
        self.table.cellClicked.connect(self.cellClick)

    '''def add(self):
        self.window = addContactWindow()
        centerWindow(self.window)

    def delete(self):
        pass'''

    def butSelect(self):
        if len(self.table.selectedItems()) <> 2:
            return
        window.to_addr_Set(self.emailTo)
        self.close()
        #QtCore.QObject.connect(QtCore.SIGNAL("clicked()"), QtGui.qApp, QtCore.SLOT("close()"))

    def cellClick(self, row,col):
        if col == 0: col = col+1
        self.emailTo = self.table.item(row, col).text()
        '''for item in range(self.table.rowCount()):
            for column in range(2):
                self.table.item(item, column).setBackgroundColor(QtGui.QColor(255, 255, 255))'''
        #self.table.setStyleSheet("QTableWidget{selection-background-color: #ff0000;}")
        '''не работает palette = QtGui.QPalette()
        palette.setColor(QtGui.QPalette.Background,QtCore.Qt.red)
        self.table.setPalette(palette)'''
        #вариант 1
        #self.table.setItemSelected(self.table.item(row, col+1 if col == 0 else col-1), True)
        #вариант 2
        #self.table.item(row, col).setBackgroundColor(QtGui.QColor(255, 127, 80))
        #self.table.item(row, col+1 if col == 0 else col-1).setBackgroundColor(QtGui.QColor(255, 127, 80))


class addContactWindow(QtGui.QWidget):
    def __init__(self, parent=None):
        super(addContactWindow, self).__init__(parent)
        uic.loadUi("C:/mail_settings_form/views/new_contact.ui", self)

        self.connect(self.saveButton, QtCore.SIGNAL('clicked()'), self.save)

    def save(self):
        addEmailInAddressBook([unicode(self.lineEditName.text()).strip(), str(self.lineEditEmail.text()).strip()])
        window.addressBook1.addRow(unicode(self.lineEditName.text()).strip(), str(self.lineEditEmail.text()).strip())
        self.close()

class changeContactWindow(addContactWindow):
    def setNewValues(self):
        self.lineEditName.setText(window.addressBook1.name)
        self.lineEditEmail.setText(window.addressBook1.emailTo)
        self.setWindowTitle(u'Изменить контакт')
        #self.connect(self.saveButton, QtCore.SIGNAL('clicked()'), self.ifContactExists)
    def save(self):
        if str(self.lineEditEmail.text()).strip() == window.addressBook1.emailTo:
            addEmailInAddressBook([unicode(self.lineEditName.text()).strip(), str(self.lineEditEmail.text()).strip()])
            window.addressBook1.table.item(window.addressBook1.row, 0).setText(unicode(self.lineEditName.text()).strip())
            self.close()
        else:
            window.addressBook1.delete()
            '''r = shelve.open('abook')
            del r[str(window.addressBook1.emailTo)]
            r.close()'''
            addEmailInAddressBook([unicode(self.lineEditName.text()).strip(), str(self.lineEditEmail.text()).strip()])
            window.addressBook1.addRow(unicode(self.lineEditName.text()).strip(), unicode(self.lineEditEmail.text()).strip())
            self.close()

class MainWindow(QtGui.QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        QtGui.QWidget.__init__(self)
        uic.loadUi("C:/mail_settings_form/views/main_window.ui", self)
        self.label_attachment.hide()
        self.attachment.hide()
        self.attachmentDeleteButton.hide()
        #self.progressBar.hide()
        self.label_status.hide()

        self.connect(self.emailSelectButton, QtCore.SIGNAL("clicked()"), self.addressbook_window_show)
        self.connect(self.SentButton, QtCore.SIGNAL("clicked()"), self.sent)
        self.connect(self.attachmentDeleteButton, QtCore.SIGNAL("clicked()"), self.attachment_delete)
        self.comboBox.currentIndexChanged.connect(self.selectionchange)

        #self.get_accounts_list()
        Settings.get_accounts_list(self)

        #self.TxtFilePath = QSelectPath('file')
        #self.createActions()
        #self.menu_help.addAction(self.aboutAct)
        #self.exitAct = QtGui.QAction("E&xit", self, shortcut="Ctrl+Q",statusTip="Exit the application", triggered=self.close)

        #self.menu_file.addAction(self.exitAct)

        self.action_exit.triggered.connect(self.close)
        self.action_attachment.triggered.connect(self.add_attachment)
        self.action_settings.triggered.connect(self.settings_window_show)
        self.action_addressBook.triggered.connect(self.addressBook_window_show)
        self.action_log_show.triggered.connect(self.log_show)
        self.action_Qt.triggered.connect(self.aboutQt)
        self.action_about.triggered.connect(self.about)

        # или textEdited
        #self.comboBox.textChanged.connect(self.on_text_changed)
        self.to_addr.textChanged.connect(self.on_text_changed)
        self.subject.textChanged.connect(self.on_text_changed)
        self.attachment.textChanged.connect(self.on_text_changed)
        self.body.textChanged.connect(self.on_text_changed)

    def on_text_changed(self):
        #if self.SaveButton.isEnabled() == False:#Кнопка нажата
        self.progressBar.setRange(0,100)
        self.progressBar.setValue(0)
        self.label_status.hide()

    def createActions(self):
        self.aboutAct = QtGui.QAction("&About7", self,
                statusTip=u"Показать информацию о программе",
                triggered=self.about)

    def selectionchange(self, i):
        self.progressBar.setRange(0,100)
        self.progressBar.setValue(0)
        self.label_status.hide()
        s = LoadSettings(str(self.comboBox.currentText()))
        #self.account.setText(s['account'])
        self.mail_server = s['mail_server']
        self.mail_server_port = s['mail_server_port']
        self.login = s['login']
        self.password = s['password']
        # delete self.SaveButton.setDisabled(False)

    def clear_form(self):
        #map(lambda x: x.clear(), self.findChildren(QtGui.QLineEdit))
        for i in self.findChildren(QtGui.QLineEdit): i.clear()

    @decorator
    def sent(self):
        '''for i in self.findChildren(QtGui.QLabel): i.setStyleSheet("color: #000000;")
        if re.match("^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)$", str(self.account.text())) == None:
            self.label_6.setStyleSheet("color: #ff0000;")#или self.mail_server_port.setStyleSheet("background-color: #ff0000;")
            return
        #если порт это не целое число
        try:
            int(self.mail_server_port.text())
        except ValueError:
            self.label_3.setStyleSheet("color: #ff0000;")
            return'''
        if len(self.comboBox) == 0:
            QtGui.QMessageBox.information(None, u"Инфо",u"Нет ни одной учетной записи - перейдите в раздел \"Настройки\" и добавьте записи",buttons=QtGui.QMessageBox.Close,defaultButton=QtGui.QMessageBox.Close)
            return

        self.progressBar.setRange(0,0)

        # если нужно прикрепить вложение
        if len(self.attachment.text()) > 3:
            m = MIMEMultipart()
            m.attach(MIMEText(unicode(self.body.toPlainText()).encode("utf-8"), "", "utf-8"))
            attachment = str(self.attachment.text())
            fp = open(attachment, 'rb')
            msg = MIMEBase('application', 'octet-stream')
            msg.set_payload(fp.read())
            fp.close()
            encoders.encode_base64(msg)
            msg.add_header("Content-Disposition", "attachment", filename=attachment)
            m.attach(msg)
        else:
            m = MIMEText(unicode(self.body.toPlainText()).encode("utf-8"), "", "utf-8")

        m["From"] = str(self.comboBox.currentText())
        m["To"] = str(self.to_addr.text())
        m["Subject"] = unicode(self.subject.text()).encode("utf-8")

        if self.checkBoxSSL.isChecked():
            s = smtplib.SMTP_SSL(self.mail_server, self.mail_server_port)
        else:
            s = smtplib.SMTP(self.mail_server, self.mail_server_port)
        #s.set_debuglevel(1)
        s.login(self.login, self.password)
        try:
            s.sendmail(m["From"], m["To"], m.as_string())            
        except ValueError as e:
            print(e)

        s.quit()

        #выполнить только в случае успешной отправки сообщения
        self.progressBar.setRange(0,100)
        self.progressBar.setValue(100)
        self.label_status.show()
        '''self.comboBox.addItem(self.account.text())
        for i in self.findChildren(QtGui.QLabel): i.setStyleSheet("color: #000000;")
        self.SaveButton.setDisabled(True)'''

    def add_attachment(self):
        self.attachment.setReadOnly(True)
        path = QtGui.QFileDialog.getOpenFileName(self, u'Выберите файл')
        if path:
            if os.path.getsize(unicode(path)) > 6815744: #6.5mb размер файла в байтах
                QtGui.QMessageBox.information(None, u"Инфо",u"Размер файла не более 6,5mb",buttons=QtGui.QMessageBox.Close,defaultButton=QtGui.QMessageBox.Close)
                return
        '''if path:
            if path[-3:] == 'rar':
                QtGui.QMessageBox.information(None, u"Инфо",u"Только zip-архивы",buttons=QtGui.QMessageBox.Close,defaultButton=QtGui.QMessageBox.Close)
                return'''
        self.attachment.setText(path)
        self.label_attachment.show()
        self.attachment.show()
        self.attachmentDeleteButton.show()

    def addressbook_window_show(self):
        self.window = addressbookWindow()
        centerWindow(self.window)
        self.window.setStyleSheet("QTableWidget{selection-background-color: #9acd32;}")
        #self.window.setStyleSheet(open("./QDarkStyle2.qss","r").read())
        #self.window.setStyle("Plastique")# не работает здесь
        #self.window.setStyleSheet(open("C:/PDFConverter/skins/light-blue.qss","r").read())

    def attachment_delete(self):
        self.label_attachment.hide()
        self.attachment.hide()
        self.attachmentDeleteButton.hide()
        self.attachment.clear()

    def settings_window_show(self):
        self.window = Settings()
        #self.window.setStyleSheet(open("C:/mail_settings_form/skins/pagefold.qss","r").read())#C:/PDFConverter/skins/
        self.window.show()
        #self.window.activateWindow()
    #addressBook1
    def addressBook_window_show(self):
        self.addressBook1 = addressBook()
        #print type(self.addressBook1)
        centerWindow(self.addressBook1)
        self.addressBook1.setStyleSheet("QTableWidget{selection-background-color: #9acd32;}")
        #self.window.show()
    def log_show(self):
        subprocess.Popen(u'C:/mail_settings_form/log.log', shell = True)#log.log
    def aboutQt(self):
        QtGui.QMessageBox.aboutQt(self)

    def about(self):
        #self.infoLabel.setText("Invoked <b>Help|About</b>")
        QtGui.QMessageBox.about(self, u"О программе Mail Sender Portable", "<br>Mail Sender Portable 1.0<br><br>Copyright \xa9 2016<br>")

    def to_addr_Set(self, email):
        self.to_addr.setText(email)


if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    window = MainWindow()
    #window.setStyleSheet(open("C:/mail_settings_form/skins/pagefold.qss","r").read())
    window.show()
    #app.setStyleSheet(open("C:/PDFConverter/skins/pagefold.qss","r").read())
    sys.exit(app.exec_()) # Запускаем цикл обработки событий
